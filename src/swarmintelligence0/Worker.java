/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package swarmintelligence0;

import javax.swing.JFrame;
import mas.AbstractSwarm;
import gui.MainForm;
import javax.swing.JOptionPane;

/**
 *
 * @author martin
 */
public class Worker extends Thread{
    private final int DELAY = 20;
    
    private final MainForm frame;
    private final AbstractSwarm swarm;    
    
    private boolean isPaused = true;
    private boolean isStopped = false;
    
    private int maxIteration;
    private int maxWithoutUpdate;
    private double eps = 1e-6;
    
    public Worker(AbstractSwarm swarm, JFrame frame){
        this.frame = (MainForm)frame;
        this.swarm = swarm;
        
        this.maxIteration = MainForm.maxIterations;
        this.maxWithoutUpdate = MainForm.maxWithoutUpdate;
    }
    
    @Override
    public void run(){
        long beforeTime, timeDiff, sleep;
        double prevBestValue;
        int withoutUpdates = 0;
        
        beforeTime = System.currentTimeMillis();
        prevBestValue = swarm.gBest;
        
        while (!isStopped && swarm.iterations < maxIteration){ 
            
            if (withoutUpdates > maxWithoutUpdate){
                int choice = JOptionPane.showConfirmDialog(frame, 
                        "Best position didn't change for more than" + maxWithoutUpdate + ".\n Would you like to stop computing ?", 
                        "Without updates for too long", JOptionPane.YES_NO_OPTION);
                if (choice == JOptionPane.YES_OPTION){
                    break;
                } else {
                    withoutUpdates = 0;
                }
            }
            
            cycle();
            frame.plotPanel.repaint(); 
            frame.updateData();
            
            if (prevBestValue - swarm.gBest < eps){
                withoutUpdates++;
            } else {
                withoutUpdates = 0;
                prevBestValue = swarm.gBest;
            }            
            
            timeDiff = System.currentTimeMillis() - beforeTime;
            sleep = DELAY - timeDiff;
            
            if (sleep < 0){
                sleep = 2;
            }
            
            try{
                Thread.sleep(sleep);
            } catch (InterruptedException e){
                System.out.println(e.getMessage());
            }
            
            beforeTime = System.currentTimeMillis();            
        }       
        
        frame.stop();
    }
    
    public void finish(){
        isStopped = true;
    }
    
    private void cycle(){
        swarm.makeIteration();
    }
    
}
