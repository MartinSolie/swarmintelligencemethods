/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import mas.MASFactory;
import mas.pso.PSOSwarm;

/**
 *
 * @author martin
 */
public class PSOSetupForm extends JFrame{
    
    private static final double INERTIA_MAX = 1;
    private static final double INERTIA_MIN = 0;
    private static final double INERTIA_STEP = 0.05;
    
    private static final double COEFF_MAX = 2;
    private static final double COEFF_MIN = 0;
    private static final double COEFF_STEP = 0.01;
    
    private JSpinner inertiaSpinner = new JSpinner();
    private JSpinner cognitiveSpinner = new JSpinner();
    private JSpinner socialSpinner = new JSpinner();
    
    private JButton saveButton = new JButton("Save");
    private JButton cancelButton = new JButton("Cancel");
    
    public PSOSetupForm(){
        
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize (300, 200);
        setTitle("Setup PSO parameters");
        setLayout(new GridLayout(4,2));        
        
        SpinnerModel inertiaSM = new SpinnerNumberModel(MASFactory.kv, INERTIA_MIN, INERTIA_MAX, INERTIA_STEP);
        SpinnerModel cognitiveSM = new SpinnerNumberModel(MASFactory.particleIncrementRatio, COEFF_MIN, COEFF_MAX, COEFF_STEP);
        SpinnerModel socialSM = new SpinnerNumberModel(MASFactory.groupIncrementRatio, COEFF_MIN, COEFF_MAX, COEFF_STEP);                  
                
        inertiaSpinner.setModel(inertiaSM);        
        cognitiveSpinner.setModel(cognitiveSM);
        socialSpinner.setModel(socialSM);
        
        add(new JLabel("Inertia:"));
        add(inertiaSpinner);        
        
        add(new JLabel("Cognitive:"));
        add(cognitiveSpinner);
        
        add(new JLabel("Social:"));
        add(socialSpinner);
        
        saveButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                MASFactory.kv = (double)inertiaSpinner.getValue();
                MASFactory.particleIncrementRatio = (double)cognitiveSpinner.getValue();
                MASFactory.groupIncrementRatio = (double)socialSpinner.getValue();
                close();
            }
        });
        
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                close();
            }
        });
        
        add(saveButton);
        add(cancelButton);
    }
    
    private void close(){
        this.dispose();
    }
}
