package gui;

import functions.CustomFunction;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * Form for input different function's parameters, such as dimensions' borders
 * For predefined functions shows only form of function
 * For custom function allows to input user's function
 * @author martin
 */
public class FunctionSetupForm extends JFrame{
    private MainForm form;
    private boolean custom;
    private JFrame self;
    
    private JButton saveButton;
    private JButton cancelButton;
    private JButton copyButton;
    private JButton newFunctionButton;
    
    private JTextField functionField;
    private JTextField[] minXFields;
    private JTextField[] maxXFields;
    
    private int newAxesAmount;
    
    //for editing function's constraints
    public FunctionSetupForm (MainForm f, boolean isCustom){
        form = f;
        custom = isCustom;
        self = this;
        
        minXFields = new JTextField[form.computable.getAxesAmount()];
        maxXFields = new JTextField[form.computable.getAxesAmount()];
        
        setTitle(f.computable.getName());
        
        setSize (600,400);
        setLayout(new GridLayout (6 + 2*f.computable.getAxesAmount(), 2));
        
        add(new JLabel("Function: "));                
        functionField = new JTextField(form.computable.toString());
        functionField.setEditable(false);                
        add(functionField);
        
        add (new JLabel("Constraints:")); add(new JLabel("")); //separators
        
        for (int axis = 0; axis < form.computable.getAxesAmount(); axis++){
            add(new JLabel("min x"+ axis +" : "));
            minXFields[axis] = new JTextField(String.valueOf(form.computable.getMinX(axis)));        
            add(minXFields[axis]);

            add(new JLabel("max x"+axis+" : "));
            maxXFields[axis] = new JTextField(String.valueOf(form.computable.getMaxX(axis)));
            add(maxXFields[axis]);
        }        
        
        add (new JLabel("")); add(new JLabel("")); //separators
        
        copyButton = new JButton("Copy constraints");
        copyButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                int choice = JOptionPane.showConfirmDialog(null, "Would you like to copy constraints\nfrom x0 to all the other variables?",
                "Copy constraints", JOptionPane.YES_NO_OPTION);
                if (choice == JOptionPane.YES_OPTION){
                    for (int i = 1; i < minXFields.length; i++){
                        minXFields[i].setText(minXFields[0].getText());
                        maxXFields[i].setText(maxXFields[0].getText());
                    }
                }
            }
            
        });        
        add(copyButton); add(new JLabel(""));
                
        newFunctionButton = new JButton("New function");
        newFunctionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                String result = (String)JOptionPane.showInputDialog(null, 
                        "Amount of variables in new function:", "Create new function",
                        JOptionPane.QUESTION_MESSAGE, null, null, "2");
                if(result != null){
                    int n;
                    try {
                        n = Integer.parseInt(result);
                    } catch (NumberFormatException ex) {
                        String errorMessage = "Please, input only positive integer.";

                        JOptionPane.showMessageDialog(null, errorMessage, "Uncorrect input",
                            JOptionPane.ERROR_MESSAGE);

                        return;
                    }

                    FunctionSetupForm newFunctionForm = new FunctionSetupForm(form, n);
                    newFunctionForm.setVisible(true);

                    close();
                }
            }        
        });         
        add(newFunctionButton);
        
        add(new JLabel("")); //separators
        
        saveButton = new JButton("Save");
        saveButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                double[] minX = new double[form.computable.getAxesAmount()];
                double[] maxX = new double[form.computable.getAxesAmount()];
                
                try{
                    for (int axis = 0; axis < form.computable.getAxesAmount(); axis++){
                        minX[axis] = Double.parseDouble(minXFields[axis].getText());
                        maxX[axis] = Double.parseDouble(maxXFields[axis].getText());
                    }                    
                }catch (NumberFormatException ex){
                    String errorMessage = "Only decimal numbers"
                            + "\nFloat separator is dot (.)\nFor example: -3.5";
                    JOptionPane.showMessageDialog(null, errorMessage, "Uncorrect input",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                
                form.computable.setMinX(minX);
                form.computable.setMaxX(maxX);
                
                
                close();
            }            
        });
        add(saveButton);
        
        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                close();
            }
        });
        add(cancelButton);
    }
    
    //for creating new function
    public FunctionSetupForm(MainForm f, int axesAmount){
        form = f;
        newAxesAmount = axesAmount;
        
        minXFields = new JTextField[axesAmount];
        maxXFields = new JTextField[axesAmount];
        
        setTitle("Custom");        
        setSize (600,400);
        setLayout(new GridLayout (6 + 2*axesAmount, 2));
        
        add(new JLabel("Function: "));                
        functionField = new JTextField("");        
        add(functionField);
        
        add (new JLabel("Constraints:")); add(new JLabel("")); //separators
        
        for (int axis = 0; axis < axesAmount; axis++){
            add(new JLabel("min x"+ axis +" : "));
            minXFields[axis] = new JTextField("");        
            add(minXFields[axis]);

            add(new JLabel("max x"+axis+" : "));
            maxXFields[axis] = new JTextField("");
            add(maxXFields[axis]);
        }        
        
        add (new JLabel("")); add(new JLabel("")); //separators
        
        copyButton = new JButton("Copy constraints");
        copyButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                int choice = JOptionPane.showConfirmDialog(null, "Would you like to copy constraints\nfrom x0 to all the other variables?",
                "Copy constraints", JOptionPane.YES_NO_OPTION);
                if (choice == JOptionPane.YES_OPTION){
                    for (int i = 1; i < minXFields.length; i++){
                        minXFields[i].setText(minXFields[0].getText());
                        maxXFields[i].setText(maxXFields[0].getText());
                    }
                }
            }
            
        });        
        add(copyButton); add(new JLabel(""));        
        
        add(new JLabel("")); add(new JLabel(""));
        
        saveButton = new JButton("Save");
        saveButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                double[] minX = new double[newAxesAmount];
                double[] maxX = new double[newAxesAmount];
                
                try{
                    for (int axis = 0; axis < newAxesAmount; axis++){
                        minX[axis] = Double.parseDouble(minXFields[axis].getText());
                        maxX[axis] = Double.parseDouble(maxXFields[axis].getText());
                    }                    
                }catch (NumberFormatException ex){
                    String errorMessage = "Only decimal numbers"
                            + "\nFloat separator is dot (.)\nFor example: -3.5";
                    JOptionPane.showMessageDialog(null, errorMessage, "Uncorrect input",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                
                CustomFunction function = new CustomFunction(functionField.getText(), minX, maxX);                
                form.computable = function;
                
                close();
            }            
        });
        add(saveButton);
        
        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                close();
            }
        });
        add(cancelButton);        
    }
    
    public void close(){        
        this.dispose();
    }
}
