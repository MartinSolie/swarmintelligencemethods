package gui;

import functions.Computable;
import javax.swing.JFrame;
import org.math.plot.Plot3DPanel;

/**
 * Builds plot of given function and shows it
 * @author martin
 */
public class FunctionPlotForm extends JFrame {
    private Computable computable;    
    
    public FunctionPlotForm(Computable computable){
        this.computable = computable;
        setSize (400,300);
        setTitle(computable.getName());
        
        add(create3DPlot());
    }    
    
    /**
     * Creates function plot
     * 
     * @return  3D plot of probability to stop inside the net
     * @see     optimize3dPlot()
     */
    private Plot3DPanel create3DPlot(){
        double[][] plotData;
        
        int width, height;
        width = (int)((computable.getMaxX(0)-computable.getMinX(0))*10);
        height = (int)((computable.getMaxX(1)-computable.getMinX(1))*10);
        
        plotData = new double[width][height];       
        
        double x = computable.getMinX(0);
        double y = computable.getMinX(1);
        double delta = 0.1;
        
        for (int i = 0; i<width; i++){
            for (int j = 0; j<height;  j++){
                plotData[i][j] = computable.getFunctionValue(new double[]{x, y});
                y+=delta;
            }
            x+=delta;
            y = computable.getMinX(1);
        }        
        
        plotData = scale3DPlot(plotData, 7);
        
        Plot3DPanel plot = new Plot3DPanel();
        
        delta = (computable.getMaxX(0)-computable.getMinX(0))/plotData.length;        
        x=computable.getMinX(0);
        double[] xCoord = new double [plotData.length];
        for (int i = 0; i<xCoord.length; i++){
            xCoord[i] = x;
            x+=delta;
        }
        
        delta = (computable.getMaxX(1)-computable.getMinX(1))/plotData[0].length;
        y=computable.getMinX(1);
        double[] yCoord = new double [plotData[0].length];
        for (int i = 0; i<yCoord.length; i++){
            yCoord[i] = y;            
            y+=delta;
        }        
        
        plot.addGridPlot("Function", xCoord, yCoord, plotData);       
        
        return plot;
    }
    
    /**
     * Scales array of dots to make the plot smaller
     * 
     * @param data - data to be scaled
     * @param k - coefficient of scaling
     * @return scaled 3D plot data of probabilities to stop inside the net
     */
    
    private double[][] scale3DPlot(double[][] data, int k){
               
        double resultData[][];        
        int width;
        int height;        
        
        width = data.length / k + data.length % k;
        height = data[0].length / k + data[0].length % k;
        resultData = new double[width][height];

        for (int i = 0; i < width; i++){
            for (int j = 0; j<height; j++){
                resultData[i][j] = getZipped(i, j, k, data);
            }
        }        
        
        return resultData;
    }
    
    /**
     * Squeezes square to one number - sum of the elements of the square
     * <p>
     * _______
     * |* * * |
     * |* * * | => *      
     * |* * * | 
     *  ------  
     * 
     * @param i i-th index of the top left corner of the square, which has to be squeezed
     * @param j j-th index of the top left corner of the square, which hat to be squeezed
     * @param k coefficient of squeezing (length of the square's side)
     * @return  sum of the elements
     */
    
    private double getZipped(int i, int j, int k, double[][] data){
              
        double result = 0;
        
        for (int l = 0; l<k; l++){
            for (int m = 0; m<k; m++){
                if (i*k+l >= data.length || j*k+m >= data[0].length){
                    continue;
                }                
                result += data[i*k+l][j*k+m];
            }
        }
        
        return result;
    }
    
}
