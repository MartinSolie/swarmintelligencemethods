/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

/**
 *
 * @author martin
 */
public class plots {
    public plots(){
        /*
        int size = 50;
        
        double values[][] = new double [size][size];
        double[] x = new double [size];
        double[] y = new double [size];
        
        for (int ix = 0; ix<size; ix++){
            for (int iy = 0; iy<size; iy++){
                values[ix][iy] = ranaFunction(ix, iy);
            }
            x[ix] = ix;
            y[ix] = ix;
        }
        
        Plot3DPanel plot = new Plot3DPanel();             
        
        plot.addGridPlot("Stopped", y, x, values);
        
        add(plot);
        */
    }
    
    public final double schaffersFunction(double x, double y){
        return 0.5 + (Math.pow(Math.sin(Math.sqrt(x*x+y*y)),2)-0.5)/Math.pow((1+0.001*(x*x+y*y)),2);
    }
    
    public final double ranaFunction(double x, double y){
        return x * Math.sin(Math.sqrt(Math.abs(y + 1 - x))) * Math.cos(Math.sqrt(Math.abs(x + y + 1)))
                + (y + 1) * Math.cos(Math.sqrt(Math.abs(y + 1 - x))) * Math.sin(Math.sqrt(Math.abs(x + y + 1)));
    }
}
