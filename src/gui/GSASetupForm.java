/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import mas.MASFactory;

/**
 *
 * @author martin
 */
public class GSASetupForm extends JFrame {
    
    private static final double GDT_MAX = 1;
    private static final double GDT_MIN = 0;
    private static final double GDT_STEP = 0.01;
    
    private JTextField gTextField = new JTextField();
    private JSpinner gdtSpinner = new JSpinner();
    
    private JButton saveButton = new JButton("Save");
    private JButton cancelButton = new JButton("Cancel");
    
    public GSASetupForm(){
        
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(300,200);
        setTitle("Setup GSA parameters");
        setLayout(new GridLayout(3,2));
        
        gdtSpinner.setModel(new SpinnerNumberModel(MASFactory.gdt, GDT_MIN, GDT_MAX, GDT_STEP));
        
        gTextField.setText(MASFactory.g + "");
        gTextField.setHorizontalAlignment(SwingConstants.RIGHT);
        
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                double g, gdt;
                
                try{
                    g = Double.parseDouble(gTextField.getText());
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Not a valid number", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                
                gdt = (double)gdtSpinner.getValue();
                MASFactory.g = g;
                MASFactory.gdt = gdt;
                
                close();
            }
        });
        
        
        cancelButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                close();
            }
        
        });
        
        
        add(new JLabel("g:"));
        add(gTextField);
        add(new JLabel("gdt:"));
        add(gdtSpinner);
        add(saveButton);
        add(cancelButton);
    }
    
    public void close(){
        this.dispose();
    }
    
}
