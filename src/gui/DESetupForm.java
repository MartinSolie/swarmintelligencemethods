/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import mas.MASFactory;

/**
 *
 * @author martin
 */
public class DESetupForm extends JFrame{
    
    private static final double F_MAX = 2;
    private static final double F_MIN = 0;
    private static final double F_STEP = 0.01;
    
    private static final double P_MAX = 1;
    private static final double P_MIN = 0;
    private static final double P_STEP = 0.01;
    
    private JSpinner fSpinner = new JSpinner();
    private JSpinner pSpinner = new JSpinner();
    
    private JButton saveButton = new JButton("Save");
    private JButton cancelButton = new JButton("Cancel");
    
    public DESetupForm(){
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(300, 200);
        setTitle("Setup DE parameters");
        setLayout(new GridLayout(3, 2));
        
        fSpinner.setModel(new SpinnerNumberModel(MASFactory.F, F_MIN, F_MAX, F_STEP));
        pSpinner.setModel(new SpinnerNumberModel(MASFactory.p, P_MIN, P_MAX, P_STEP));
        
        saveButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                MASFactory.F = (double)fSpinner.getValue();
                MASFactory.p = (double)pSpinner.getValue();
                
                close();
            }
            
        });        
        
        cancelButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                close();
            }
        
        });
        
        add(new JLabel("F:"));
        add(fSpinner);
        add(new JLabel("P:"));
        add(pSpinner);
        add(saveButton);
        add(cancelButton);
    }
    
    public void close(){
        this.dispose();
    }
}
