/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import functions.Computable;
import functions.CustomSystem;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 *
 * @author martin
 */
public class SystemSetupForm extends JFrame {
    
    private JTextField[] equationsFields;
    private JTextField[] minXFields;
    private JTextField[] maxXFields;
    
    private JButton saveButton;
    private JButton cancelButton;
        
    private int eqAmount;    
    private CustomSystem system;
        
    private double[] minX, maxX;
    
    public SystemSetupForm(Computable sys){
        
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle("System Setup Form");
        setLayout(new BorderLayout());
        setSize(400, 600);
        
        system = (CustomSystem)sys;
        
        equationsFields = new JTextField[system.getEquationsAmount()];
        minXFields = new JTextField[system.getAxesAmount()];
        maxXFields = new JTextField[system.getAxesAmount()];
        
        for (int i = 0; i < equationsFields.length; i++){
            equationsFields[i] = new JTextField(system.getEquations()[i].toString());
            equationsFields[i].setEditable(false);
        }
        
        for (int i = 0; i < minXFields.length; i++){
            minXFields[i] = new JTextField(system.getMinX(i) + "");
            maxXFields[i] = new JTextField(system.getMaxX(i) + "");
        }        
        
        JPanel equationPanel = new JPanel();
        equationPanel.setLayout(new GridLayout(equationsFields.length, 1));
        for (JTextField equationsField : equationsFields) {
            equationPanel.add(equationsField);
        }
        equationPanel.setMaximumSize(new Dimension(1920, equationsFields.length * 50));
        
        JScrollPane equationScrollPane = new JScrollPane(equationPanel);
        equationScrollPane.setBorder(BorderFactory.createTitledBorder("Equations:"));
        
        JPanel constraintsPanel = new JPanel();
        constraintsPanel.setLayout (new GridLayout(3 * minXFields.length, 2));
        for (int i = 0; i < minXFields.length; i++){
            constraintsPanel.add(new JLabel("x"+i));
            constraintsPanel.add(new JSeparator(SwingConstants.HORIZONTAL));
            constraintsPanel.add(new JLabel("min:"));
            constraintsPanel.add(minXFields[i]);
            constraintsPanel.add(new JLabel("max:"));
            constraintsPanel.add(maxXFields[i]);
        }
        
        JScrollPane constrScrollPane = new JScrollPane(constraintsPanel);
        constrScrollPane.setBorder(BorderFactory.createTitledBorder("Constraints:"));        
        
        JPanel allPanel = new JPanel();
        allPanel.setLayout(new GridLayout(2, 1));
        allPanel.add(equationScrollPane);
        allPanel.add(constrScrollPane);
        
        add(allPanel, BorderLayout.CENTER);
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(1, 2));
        
        saveButton = new JButton("Save");
        saveButton.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                minX = new double[minXFields.length];
                maxX = new double[maxXFields.length];
                
                parseValues();
                
                system.setMaxX(maxX);
                system.setMinX(minX);
                
                close();
            }                        
        });
        
        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                close();
            }            
        });
        buttonPanel.add(saveButton);
        buttonPanel.add(cancelButton);
        
        add(buttonPanel, BorderLayout.SOUTH);
    }

    private void parseValues(){
        try{
            for (int i = 0; i < minXFields.length; i++){
                minX[i] = Double.parseDouble(minXFields[i].getText());
                maxX[i] = Double.parseDouble(maxXFields[i].getText());
            }
        } catch (NumberFormatException ex) {
            String errorMessage = "Only decimal numbers"
                            + "\nFloat separator is dot (.)\nFor example: -3.5";
            JOptionPane.showMessageDialog(null, errorMessage, "Uncorrect input",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void close(){
        this.dispose();
    }
}
