/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import mas.MASFactory;

/**
 *
 * @author martin
 */
public class MSOSetupForm extends JFrame{
    private static final double INERTIA_MAX = 1;
    private static final double INERTIA_MIN = 0;
    private static final double INERTIA_STEP = 0.05;
    
    private static final double COEFF_MAX = 2;
    private static final double COEFF_MIN = 0;
    private static final double COEFF_STEP = 0.01;
    
    private static final int SWARMS_MAX = 100;
    private static final int SWARMS_MIN = 1;
    private static final int SWARMS_STEP = 1;
    
    private JSpinner inertiaSpinner = new JSpinner();
    private JSpinner cognitiveSpinner = new JSpinner();
    private JSpinner socialSpinner = new JSpinner();
    private JSpinner globalSpinner = new JSpinner();
    private JSpinner swarmsSpinner = new JSpinner();
    
    private JButton saveButton = new JButton("Save");
    private JButton cancelButton = new JButton("Cancel");
    
    public MSOSetupForm(){
        
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize (300, 400);
        setTitle("Setup PSO parameters");
        setLayout(new GridLayout(6,2));        
                
        inertiaSpinner.setModel(new SpinnerNumberModel(MASFactory.msoKV, INERTIA_MIN, INERTIA_MAX, INERTIA_STEP));        
        cognitiveSpinner.setModel(new SpinnerNumberModel(MASFactory.msoParticleIncrementRatio, COEFF_MIN, COEFF_MAX, COEFF_STEP));
        socialSpinner.setModel(new SpinnerNumberModel(MASFactory.msoGroupIncrementRatio, COEFF_MIN, COEFF_MAX, COEFF_STEP));
        globalSpinner.setModel(new SpinnerNumberModel(MASFactory.globalIncrementRatio, COEFF_MIN, COEFF_MAX, COEFF_STEP));
        swarmsSpinner.setModel(new SpinnerNumberModel(MASFactory.nSwarms, SWARMS_MIN, SWARMS_MAX, SWARMS_STEP));
        
        add(new JLabel("Inertia:"));
        add(inertiaSpinner);        
        
        add(new JLabel("Cognitive:"));
        add(cognitiveSpinner);
        
        add(new JLabel("Social:"));
        add(socialSpinner);
        
        add(new JLabel("Global:"));
        add(globalSpinner);
        
        add(new JLabel("Number of swarms:"));
        add(swarmsSpinner);
        
        saveButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                MASFactory.msoKV = (double)inertiaSpinner.getValue();
                MASFactory.msoParticleIncrementRatio = (double)cognitiveSpinner.getValue();
                MASFactory.msoGroupIncrementRatio = (double)socialSpinner.getValue();
                MASFactory.globalIncrementRatio = (double)globalSpinner.getValue();
                MASFactory.nSwarms = (int)swarmsSpinner.getValue();
                
                close();
            }
        });
        
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                close();
            }
        });
        
        add(saveButton);
        add(cancelButton);
    }
    
    private void close(){
        this.dispose();
    }
}
