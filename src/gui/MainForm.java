
package gui;

import functions.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import mas.AbstractSwarm;
import mas.MASFactory;
import swarmintelligence0.Worker;

/**
 *
 * @author martin
 */
public class MainForm extends JFrame{
    public PlotPanel plotPanel;
    private JPanel startStopPanel;
    private JPanel settingsPanel;  
    private JPanel resultPanel;
    
    private JComboBox methodsComboBox;
    private JComboBox functionsComboBox;
    private JTextField agentsAmountField = new JTextField("100");
    private JTextArea coordinateArea = new JTextArea();
    
    private JLabel iterLabel = new JLabel("0");
    private JLabel bestValueLabel = new JLabel("N/A");
    
    private JButton startButton;
    private JButton stopButton;
    private JButton setupMethodButton;
    private JButton aboutMethodButton;
    private JButton setupFunctionButton;
    private JButton plotFunctionButton;    
    
    private AbstractSwarm swarm;
    public Computable computable;
    private Worker worker;
    
    private double minX;
    private double maxX;
    private String funcStr;
    
    public static int maxIterations = 1000;
    public static int maxWithoutUpdate = 100;
    
    private final String[] methods = {
        "Particle Swarm",
        "Glowworm Swarm",
        "Differential Evolution",
        "Gravitational Search",
        "Multi-swarm optimization"};
    
    private final String[] functions = {
        "Sphere",
        "Schaffers F6",
        "Rana",
        "Rastrigins",
        "Shekel",
        "System",
        "Water System",
        "Simple System",        
        "Custom function",
        "Custom system"};
    
    public MainForm() {
        minX = -10;
        maxX = 10;
        funcStr = "(_x0_-1)^2 + _x1_^2 + _x2_^2";
        
        computable = new Sphere(minX, maxX);
        
        this.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent ev){
                System.exit(0);
            }
        }); 
        
        setTitle("Swarm Intelligence");
        setLayout(new BorderLayout());
        
        plotPanel = new PlotPanel();
        add(plotPanel, BorderLayout.CENTER);
        
        startStopPanel = new JPanel();
        startStopPanel.setLayout(new GridLayout(1, 2));
        
        startButton = new JButton("Start");
        startButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                start();
            }
        });
        
        stopButton = new JButton("Stop");
        stopButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                stop();
            }
        });
        stopButton.setEnabled(false);
        
        startStopPanel.add(startButton);
        startStopPanel.add(stopButton);
                
        add(startStopPanel, BorderLayout.SOUTH);
        
        settingsPanel = new JPanel();
        settingsPanel.setLayout(new GridLayout(11,1));
        
        methodsComboBox = new JComboBox(methods);
        functionsComboBox = new JComboBox(functions);
        functionsComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                if (ie.getStateChange() == ItemEvent.SELECTED){
                    initComputable();
                }
            }    
        });
        
        
        setupMethodButton = new JButton("Setup method");
        setupMethodButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {  
                JFrame setupForm = null;
                
                switch(methodsComboBox.getSelectedIndex()){
                    case 0: setupForm = new PSOSetupForm();
                        break;
                    case 1: setupForm = new GSOSetupForm();
                        break;
                    case 2: setupForm = new DESetupForm();
                        break;
                    case 3: setupForm = new GSASetupForm();
                        break;
                    case 4: setupForm = new MSOSetupForm();
                        break;
                    default: JOptionPane.showMessageDialog(null, "Unknown error with setup forms", "Unknown", JOptionPane.ERROR);                
                        return;
                }
                
                setupForm.setVisible(true);
            }            
        });        
        
        aboutMethodButton = new JButton("About method");
        aboutMethodButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        setupFunctionButton = new JButton("Setup function");
        setupFunctionButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                setupFunction();
                if (computable instanceof CustomFunction){
                    funcStr = computable.toString();
                    functionsComboBox.setSelectedIndex(8);                    
                }
            }
        
        });
        
        plotFunctionButton = new JButton("Plot function");
        plotFunctionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (computable == null)
                    initComputable();
                
                if (computable.getAxesAmount() != 2 ){
                    JOptionPane.showMessageDialog(null, "Can create only 3D functions",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                FunctionPlotForm fpf = new FunctionPlotForm(computable);
                fpf.setVisible(true);
            }
        });
        
        settingsPanel.add(new JLabel("Method: "));
        settingsPanel.add(methodsComboBox);
        
        settingsPanel.add(aboutMethodButton);
        settingsPanel.add(setupMethodButton);        
        
        settingsPanel.add(new JLabel ("Number of agents: "));
        settingsPanel.add(agentsAmountField);
        
        settingsPanel.add(new JLabel(""));
        
        settingsPanel.add(new JLabel("Functions: "));
        settingsPanel.add(functionsComboBox);
        
        settingsPanel.add(plotFunctionButton);
        settingsPanel.add(setupFunctionButton);        
        
        add(settingsPanel, BorderLayout.WEST);
        
        resultPanel = new JPanel();
        resultPanel.setLayout(new GridLayout(3,2));
        resultPanel.add(new JLabel("Iteration: "));
        resultPanel.add(iterLabel);
        resultPanel.add(new JLabel("Best value: "));
        resultPanel.add(bestValueLabel);
        resultPanel.add(new JLabel("Best value pos:"));
        resultPanel.add(coordinateArea);
        
        add(resultPanel, BorderLayout.EAST);
        
        pack();              
        
        this.setVisible(true);        
    }
    
    public AbstractSwarm getSwarm(){
        return swarm;
    }
    
    public String getFuncStr(){
        return funcStr;
    }
    
    public double getMinX(){
        return minX;
    }
    
    public double getMaxX(){
        return maxX;
    }
    
    public void setSwarm (AbstractSwarm swarm){
        this.swarm = swarm;
    }
    
    public void setFuncStr(String funcStr){
        this.funcStr = funcStr;
    }
    
    public void setMinX(double minX){
        this.minX = minX;
    }
    
    public void setMaxX(double maxX){
        this.maxX = maxX;
    }
    
    public void updateData(){
        iterLabel.setText(String.valueOf(swarm.iterations));
        bestValueLabel.setText(String.format("%.5f", swarm.gBest));
        
        StringBuilder sb = new StringBuilder();                
        for (int i = 0; i < swarm.gBestX.length; i++){
            sb.append("x").append(String.valueOf(i))
                    .append(": ").append(String.format("%.2f", swarm.gBestX[i]))
                    .append("\n");
        }
        coordinateArea.setText(sb.toString());        
    }
  
    public void initComputable(){       
        switch(functionsComboBox.getSelectedIndex()){
            case 0: computable = new Sphere (minX, maxX);
                break;
            case 1: computable = new Schaffers(minX, maxX);
                break;
            case 2: computable = new Rana (minX, maxX);
                break;
            case 3: computable = new Rastrigins(minX, maxX);
                break;
            case 4: computable = new Shekel();
                break;
            case 5: computable =  new EquationSystem();
                break;
            case 6: computable = new WaterSystem();
                break;
            case 7: computable = new SimpleSystem();
                break;                
            case 8: { //if you'll change here - change in Setup Function button
                    computable = new CustomFunction(funcStr, minX, maxX);
                } break;
            case 9: {
                    String[] strEquations = {
                                                "2 * _x0_ - 3 * _x1_ + _x2_ + 1",
                                                "5 * _x0_ + 2 * _x1_ - _x2_",
                                                "_x0_ - _x1_ + 2 * _x2_ - 3"
                                            };
                    double[] minX = {-10, -10, -10};
                    double[] maxX = {10, 10, 10};
                    
                    computable = new CustomSystem(strEquations, minX, maxX);
                } break;
        }
    }
  
    public void setupFunction(){
        JFrame fsf;
        
        if (functionsComboBox.getSelectedIndex() == 8){
            fsf = new FunctionSetupForm(this, true);                    
        } else if (functionsComboBox.getSelectedIndex() == 9){
            fsf = new SystemSetupForm(computable);
        } else {
            fsf = new FunctionSetupForm(this, false);
        }
                
        fsf.setVisible(true);
    }    
    
    public void initSwarm(int N){
        switch(methodsComboBox.getSelectedIndex()){
            case 0: swarm = MASFactory.getSwarm(MASFactory.SwarmType.PSO, computable, N);
                break;
            case 1: swarm = MASFactory.getSwarm(MASFactory.SwarmType.GSO, computable, N);
                break;
            case 2: swarm = MASFactory.getSwarm(MASFactory.SwarmType.DE, computable, N);
                break;
            case 3: swarm = MASFactory.getSwarm(MASFactory.SwarmType.GSA, computable, N);
                break;
            case 4: swarm = MASFactory.getSwarm(MASFactory.SwarmType.MSO, computable, N);
                break;
            default: JOptionPane.showMessageDialog(this, "Unknown error with methods", "Unknown", JOptionPane.ERROR);                
        }
    }
    
    public void start(){
        int N;
        try{
            N = Integer.parseInt(agentsAmountField.getText());
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Not a valid number", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        //initComputable();        
        initSwarm(N);                        
        
        startButton.setEnabled(false);
        stopButton.setEnabled(true);
        
        plotPanel.setSwarm(swarm);
        
        worker = new Worker(swarm, this);
        worker.start();
    }
    
    public void stop(){        
        if (worker.isAlive()){
            worker.finish();
        }
        
        startButton.setEnabled(true);
        stopButton.setEnabled(false);
    }    
}
