/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.concurrent.ThreadLocalRandom;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import mas.AbstractAgent;
import mas.AbstractSwarm;

/**
 *
 * @author martin
 */
public class PlotPanel extends JPanel{
    
    private final int DELAY = 20;
       
    private AbstractSwarm swarm;
    private Thread animator;
    
    private double kx;
    private double ky;
    private double w;
    private double h;
    
    public PlotPanel(){
        setPreferredSize(new Dimension(400, 400));
        setDoubleBuffered(true);
        setBackground(Color.WHITE); 
        /*
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
                JOptionPane.showMessageDialog(null, me.getX()+" "+me.getY(), "Yes, really", JOptionPane.ERROR_MESSAGE);
            }

            @Override
            public void mousePressed(MouseEvent me) {
                
            }

            @Override
            public void mouseReleased(MouseEvent me) {
                
            }

            @Override
            public void mouseEntered(MouseEvent me) {
                
            }

            @Override
            public void mouseExited(MouseEvent me) {
                
            }
        });
        */
    }
    
    public void setSwarm (AbstractSwarm swarm){
        this.swarm = swarm;
    }
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        
        if (swarm == null) return;
        
        Dimension size = getSize();
        w = size.getWidth();
        h = size.getHeight();
        kx = w/(swarm.computable.getMaxX(0) - swarm.computable.getMinX(0));
        ky = h/(swarm.computable.getMaxX(1) - swarm.computable.getMinX(1));
        
        drawNet(g);
        drawAgents(g);
    }
    
    private void drawAgents(Graphics g){
        Graphics2D g2d = (Graphics2D)g;
        
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, 
                RenderingHints.VALUE_ANTIALIAS_ON);
        
        rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        
        g2d.setRenderingHints(rh);        
        g2d.setStroke(new BasicStroke(1));
        
        /*
            int R = ThreadLocalRandom.current().nextInt(0, 250);
            int G = ThreadLocalRandom.current().nextInt(0, 250);
            int B = ThreadLocalRandom.current().nextInt(0, 250);

            g2d.setColor(new Color(R, G, B));
        */
        
        g2d.setColor(Color.RED);
        for (AbstractAgent agent : swarm.agents){                    
            //g2d.drawOval((int)((agent.getX(0)+Math.abs(swarm.computable.getMinX(0)))*kx),
            //        (int)((h-(agent.getX(1)+Math.abs(swarm.computable.getMinX(1)))*ky)), 3, 3);
            g2d.fillRect((int)((agent.getX(0)+Math.abs(swarm.computable.getMinX(0)))*kx),
                            (int)((h-(agent.getX(1)+Math.abs(swarm.computable.getMinX(1)))*ky)), 3, 3);
            //System.out.println(agent.getX()+" "+agent.getY());
        }
    }
    
    public void drawNet(Graphics g){
        Graphics2D g2d = (Graphics2D)g;
        
        g2d.setColor(Color.GRAY);
        Dimension size = getSize();
        int w = (int)size.getWidth();
        int h = (int)size.getHeight();
                
        g2d.setStroke(new BasicStroke(1));
        g2d.drawLine(0+2, 0, 0+2, h);
        g2d.drawString(String.valueOf(swarm.computable.getMinX(0)), 3, h);
        g2d.drawString(String.valueOf(swarm.computable.getMaxX(0)), w-50, h);
        g2d.drawLine(w-2, 0, w-2, h);
        
        if (swarm.computable.getMinX(0)*swarm.computable.getMaxX(0) < 0){
            double zeroW = Math.abs(swarm.computable.getMinX(0)) * kx;
            g2d.drawLine((int)zeroW, 0, (int)zeroW,h);
        }
        
        if (swarm.computable.getMinX(1) * swarm.computable.getMaxX(1) < 0){
            double zeroH = Math.abs(swarm.computable.getMinX(1)) * ky;
            g2d.drawLine(0, h-(int)zeroH, w, h-(int)zeroH);
        }
    }     
}
