/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import mas.MASFactory;

/**
 *
 * @author martin
 */
    public class GSOSetupForm extends JFrame {
        private static final double COEF_MAX = 0.99;
        private static final double COEF_MIN = 0.01;
        private static final double COEF_STEP = 0.01;
        
        private JTextField stepField = new JTextField();
        private JTextField neighboursField = new JTextField();
        private JTextField sensorField = new JTextField();
        private JTextField luciferinAmountField = new JTextField();
        private JTextField decisionRangeField = new JTextField();
        
        private JSpinner roSpinner = new JSpinner();
        private JSpinner gammaSpinner = new JSpinner();
        private JSpinner betaSpinner = new JSpinner();       
        
        private JCheckBox updateRangeBox = new JCheckBox("");
        
        private JButton saveButton = new JButton("Save");
        private JButton cancelButton = new JButton("Cancel");
        
        public GSOSetupForm(){
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            setSize(300, 500);
            setTitle("Setup GSO parameters");
            setLayout(new GridLayout(9, 2));
            
            roSpinner.setModel(new SpinnerNumberModel(MASFactory.ro, COEF_MIN, COEF_MAX, COEF_STEP));
            gammaSpinner.setModel(new SpinnerNumberModel(MASFactory.gamma, COEF_MIN, COEF_MAX, COEF_STEP));
            betaSpinner.setModel(new SpinnerNumberModel(MASFactory.beta, COEF_MIN, COEF_MAX, COEF_STEP));
            
            stepField.setText(MASFactory.s + "");            
            neighboursField.setText(MASFactory.nt + "");            
            sensorField.setText(MASFactory.rMax + "");           
            luciferinAmountField.setText(MASFactory.luciferinAmount + "");            
            decisionRangeField.setText(MASFactory.rDecision + "");
            
            
            stepField.setHorizontalAlignment(SwingConstants.RIGHT);
            neighboursField.setHorizontalAlignment(SwingConstants.RIGHT);
            sensorField.setHorizontalAlignment(SwingConstants.RIGHT);
            luciferinAmountField.setHorizontalAlignment(SwingConstants.RIGHT);
            decisionRangeField.setHorizontalAlignment(SwingConstants.RIGHT);
            
            updateRangeBox.setSelected(MASFactory.dynamicRange);
            updateRangeBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    neighboursField.setEnabled(updateRangeBox.isSelected());
                    betaSpinner.setEnabled(updateRangeBox.isSelected());
                }
            });
            
            neighboursField.setEnabled(updateRangeBox.isSelected());
            betaSpinner.setEnabled(updateRangeBox.isSelected());
            
            saveButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent ae) {
                    double luciferinAmount = 0, ro = 0, gamma = 0, beta = 0, rMax = 0, rDecision = 0, s = 0;
                    int nt = 0;
                    
                    try {
                        luciferinAmount = Double.parseDouble(luciferinAmountField.getText());
                        rMax = Double.parseDouble(sensorField.getText());
                        rDecision = Double.parseDouble(decisionRangeField.getText());
                        s = Double.parseDouble(stepField.getText());
                        
                        if (updateRangeBox.isSelected())
                            nt = Integer.parseInt(neighboursField.getText());                        
                        
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, ex.getMessage(), "Not a valid number", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    
                    ro = (double)roSpinner.getValue();
                    gamma = (double)gammaSpinner.getValue();
                    beta = (double)betaSpinner.getValue();
                    
                    MASFactory.luciferinAmount = luciferinAmount;
                    MASFactory.ro = ro;
                    MASFactory.gamma = gamma;
                    MASFactory.beta = beta;
                    MASFactory.rMax = rMax;
                    MASFactory.rDecision = rDecision;
                    
                    MASFactory.dynamicRange = updateRangeBox.isSelected();
                    if (updateRangeBox.isSelected())
                        MASFactory.nt = nt;
                    
                    
                    close();
                }            
            });  
            
            cancelButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent ae) {
                    close();
                }
            
            });
            
            add(new JLabel("Ro:"));
            add(roSpinner);
            add(new JLabel("Gamma:"));
            add(gammaSpinner);
            add(new JLabel("Luciferin:"));
            add(luciferinAmountField);
            add(new JLabel("Sensor Range:"));
            add(sensorField);
            add(new JLabel("Decision Range:"));
            add(decisionRangeField);            
            add(new JLabel("Dynamical DR:"));
            add(updateRangeBox);
            add(new JLabel("Beta:"));
            add(betaSpinner);
            add(new JLabel("Desirable neighbours:"));
            add(neighboursField);
            add(saveButton);
            add(cancelButton);            
        }
        
        public void close(){
            this.dispose();
        }
}
