/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author martin
 */
public class PicturePanel extends JPanel {
    
    private Image img;
    private final String path = "/home/martin/mcreed.jpg";
    private JPanel panel = this;
    
    public PicturePanel(){
        initPicture();
    }
    
    public void initPicture(){
        loadImage();
        
        int w = img.getWidth(this);
        int h = img.getWidth(this);
        setPreferredSize(new Dimension(w,h));        
    }
    
    private void loadImage(){
        ImageIcon ii = new ImageIcon(path);
        img = ii.getImage();
    }
    
    @Override
    public void paintComponent(Graphics g){
        g.drawImage(img, 0, 0, null);
        
        g.clearRect(0, 0, this.getWidth(), this.getHeight());
        
        int dstx1, dsty1, dstx2, dsty2;                
        
        dstx1 = panel.getWidth()/2 - img.getWidth(this)/2;
        dsty1 = panel.getHeight()/2 - img.getHeight(this)/2;
        dstx2 = panel.getWidth()/2 + img.getWidth(this)/2;
        dsty2 = panel.getHeight()/2 + img.getHeight(this)/2;
        
        dstx1 = dstx1 >= 0 ? dstx1 : 0;
        dsty1 = dsty1 >= 0 ? dsty1 : 0;
        dstx2 = dstx2 < panel.getWidth() ? dstx2:panel.getWidth();
        dsty2 = dsty2 < panel.getHeight() ? dsty2:panel.getHeight();
        /*
        double k = (double)img.getWidth()/img.getHeight();
        if (Math.abs(k-(double)dstx2/dsty2) > 1e-3){
            if (dstx2<=dsty2){
                dsty2 =  dstx2 * 1/(int)k;
            } else {
                dstx2 = dsty2 * (int)k;
            }
                
        }
        */
        g.drawImage(img, 
                dstx1, dsty1, dstx2, dsty2, 
                0, 0, img.getWidth(this), img.getHeight(this), 
                null);
    }
    
}
