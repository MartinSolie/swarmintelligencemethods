package functions;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import javax.swing.JOptionPane;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

/**
 * Class allows to input target functions as String parameter.
 * @author martin
 */
public class CustomFunction implements Computable{
    /**
     * String representation of target function
     */
    private String strExpression;
    
    /**
     * Expression representation of target function
     */
    private Expression expression;
    
    /**
     * Minimal values of each parameter
     */
    private double[] minX;
    
    /**
     * Maximal values of each parameter
     */
    private double[] maxX;
    
    /**
     * List of classes, which sets connection between name of variable and it's index
     */
    private ArrayList<ParamPair> params = new ArrayList<>();
    
    public CustomFunction(String equation, double[] minX, double[] maxX){
        this.strExpression = equation;
        this.minX = minX;
        this.maxX = maxX;
        parseParams();
        parseExpression();
    }
    
    /**
     * Sets all min and max constraints to the same value
     * @param equation
     * @param minXvalue Value for low border
     * @param maxXvalue Value for upper border
     */
    public CustomFunction(String equation, double minXvalue, double maxXvalue){
        this.strExpression = equation;
        
        parseParams();
        parseExpression();
        
        this.minX = new double[params.size()];
        for (int i = 0; i<minX.length; i++){
            minX[i] = minXvalue;
        }
        this.maxX = new double[params.size()];
        for (int i = 0; i<maxX.length; i++){
            maxX[i] = maxXvalue;
        }
    }
    
    @Override
    public double getFunctionValue(double[] x) {
        for (ParamPair p : params){
                expression.setVariable(p.name, x[p.index]);                
            }
        return expression.evaluate();
    }

    @Override
    public double getMinX(int axis) {
        return minX[axis];
    }

    @Override
    public double getMaxX(int axis) {
        return maxX[axis];
    }
    
    /**
     * Tries to parse parameters
     * @return true if parsing was successful, false otherwise
     */
    public final boolean parseParams(){
        int i = strExpression.indexOf('_');
        String strIndex;
        int index;
        while (i != -1){
            strIndex = strExpression.substring(i+2, strExpression.indexOf('_', i+1));
            try{
                index = Integer.valueOf(strIndex);
            } catch (NumberFormatException e){
                JOptionPane.showMessageDialog(null, "Invalid parameter index value", "Index NaN", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            ParamPair pp = new ParamPair(strExpression.substring(i, strExpression.indexOf('_',i+1)+1), index);
            if (!params.contains(pp))
                params.add(pp);
            
            i = strExpression.indexOf('_', i+1);    //moves to the end of parameter's name
            i = strExpression.indexOf('_', i+1);    //moves to the next parameter
        }
        return true;
    }
    
    /**
     * Parses equation to expression representation, binding all parameters
     */
    private void parseExpression(){
        try{
            ExpressionBuilder eb = new ExpressionBuilder(strExpression);            
            for (ParamPair p : params){
                eb.variable(p.name);
            }
            expression = eb.build();
        } catch(Exception exc){
            System.out.println(exc.getMessage());            
        }
    }
    
    @Override
    public int getAxesAmount() {
        return params.size();
    }

    @Override
    public String getName() {
        return "Custom function";
    }
    
    @Override
    public String toString(){
        return strExpression;
    }
    
    @Override
    public void setMinX(double[] newMinX) {
        this.minX = newMinX;        
    }

    @Override
    public void setMaxX(double[] newMaxX) {
        this.maxX = newMaxX;
    }
    
    /**
     * Class represents connection between name of parameter and it's index
     */
    private class ParamPair{
        public String name;
        public int index;
        
        public ParamPair(String name, int index){
            this.name = name;
            this.index = index;
        }
        
        @Override
        public boolean equals(Object o){
            if (o != null && o instanceof ParamPair){
                ParamPair second = (ParamPair)o;
                return this.index == second.index;
            }
            return false;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 37 * hash + Objects.hashCode(this.name);
            hash = 37 * hash + this.index;
            return hash;
        }
    }
    
}
