package functions;

/**
 *  Interface for classes, which may be used for position evaluation of the agent
 *  in the problem space. (Actually class represents problem space).
 *  
 * @author martin
 */
public interface Computable {    
    
    /**
     * Evaluates agent in the coordinates it stands
     * @param x - coordinates of agent
     * @return value of given function in given coordinates
     */
    public double getFunctionValue(double[] x);
    
    /**
     * Allows to get lower bound of axis
     * @param axis index of axis
     * @return lower bound of axis with given index
     */
    public double getMinX(int axis);
    
    /**
     * Allows to get upper bound of axis
     * @param axis index of axis
     * @return upper bound of axis with given index
     */
    public double getMaxX(int axis);    
    
    /**
     * Allows to get number of dimensions
     * @return number of dimensions in problem space
     */
    public int getAxesAmount();
    
    /**
     * To identify your function override this method.
     * @return 
     */
    public String getName();

    /**
     * Allows to set minimal values for parameters.
     * @param newMinX 
     */
    public void setMinX(double[] newMinX);
    
    /**
     * Allows to set maximal values for parameters.
     * @param newMaxX 
     */
    public void setMaxX(double[] newMaxX);
}