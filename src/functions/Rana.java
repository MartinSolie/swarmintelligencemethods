package functions;

/**
 *
 * @author martin
 */
public class Rana implements Computable {
    private double[] minX;
    private double[] maxX;    
    
    public Rana (double[] minX, double[] maxX){
        this.minX = minX;
        this.maxX = maxX;
    }
    
    public Rana (double minXValue, double maxXValue){
        this.minX = new double[getAxesAmount()];
        this.maxX = new double[getAxesAmount()];
        
        for (int i = 0; i<this.minX.length; i++){
            this.minX[i] = minXValue;
            this.maxX[i] = maxXValue;
        }
    }
    
    @Override
    public double getFunctionValue(double[] x) {
        return x[0] * Math.sin(Math.sqrt(Math.abs(x[1] + 1 - x[0]))) * Math.cos(Math.sqrt(Math.abs(x[0] + x[1] + 1)))
                + (x[1] + 1) * Math.cos(Math.sqrt(Math.abs(x[1] + 1 - x[0]))) * Math.sin(Math.sqrt(Math.abs(x[0] + x[1] + 1)));
    }

    @Override
    public double getMinX(int axis) {
        return minX[axis];
    }

    @Override
    public double getMaxX(int axis) {
        return maxX[axis];
    }   

    @Override
    public final int getAxesAmount() {
        return 2;
    }
    
    @Override
    public String getName(){
        return "Rana";
    }

    public String getFormula() {
        return "_x0_ * sin(sqrt(abs(_x1_ + 1 - _x0_))) * cos(sqrt(abs(_x0_ + _x1_ + 1)))"
                +" + (_x1_ + 1) * cos(sqrt(abs(_x1_ + 1 - _x0_))) * sin(sqrt(abs(_x0_ + _x1_ + 1)))";
    }

    @Override
    public void setMinX(double[] newMinX) {
        this.minX = newMinX;        
    }

    @Override
    public void setMaxX(double[] newMaxX) {
        this.maxX = newMaxX;
    }
    
    @Override
    public String toString(){
        return "_x0_ * sin(sqrt(abs(_x1_ + 1 - _x0_))) * cos(sqrt(abs(_x0_ + _x1_ + 1)))"
                +" + (_x1_ + 1) * cos(sqrt(abs(_x1_ + 1 - _x0_))) * sin(sqrt(abs(_x0_ + _x1_ + 1)))";
    }
}
