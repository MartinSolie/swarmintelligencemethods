/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

/**
 *
 * @author martin
 */
public class Rastrigins implements Computable {
    
    private double[] minX;
    private double[] maxX;
    
    public Rastrigins(double[] minX, double[] maxX){
        this.minX = minX;
        this.maxX = maxX;
    }
    
    public Rastrigins(double minXValue, double maxXValue){
        this.minX = new double[getAxesAmount()];
        this.maxX = new double[getAxesAmount()];
        
        for (int i = 0; i<this.minX.length; i++){
            this.minX[i] = minXValue;
            this.maxX[i] = maxXValue;
        }
    }
    
    @Override
    public double getFunctionValue(double[] x) {
        return 20 
                + (x[0]*x[0]-10*Math.cos(2*Math.PI*x[0])) 
                + (x[1]*x[1]-10*Math.cos(2*Math.PI*x[1]));
    }

    @Override
    public double getMinX(int axis) {
        return minX[axis];
    }

    @Override
    public double getMaxX(int axis) {
        return maxX[axis];
    }

    @Override
    public final int getAxesAmount() {
        return 2;
    }
    
    
    @Override
    public String getName(){
        return "Rastrigins";
    }

    @Override
    public void setMinX(double[] newMinX) {
        this.minX = newMinX;        
    }

    @Override
    public void setMaxX(double[] newMaxX) {
        this.maxX = newMaxX;
    }
    
    @Override
    public String toString(){
        return "20 + _x0_ ^ 2 - 10 * cos(2 * pi * _x0_) +_x1_ ^ 2 - 10 * cos(2 * pi * _x1_)";
    }
}
