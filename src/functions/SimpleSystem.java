/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

/**
 *
 * @author martin
 */
public class SimpleSystem implements Computable{

    @Override
    public double getFunctionValue(double[] x) {
        double error = 0;
        error += Math.abs(-x[0]*(x[0]+1)+2*x[1] - 18);
        error += Math.abs(x[0] + (x[1] - 6) * (x[1] - 6) - 26);
        return error;
    }

    @Override
    public double getMinX(int axis) {
        return -20;
    }

    @Override
    public double getMaxX(int axis) {
        return 20;
    }

    @Override
    public int getAxesAmount() {
        return 2;
    }

    @Override
    public String getName() {
        return "Simple System";
    }

   @Override
    public void setMinX(double[] newMinX) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setMaxX(double[] newMaxX) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    } 
    
}
