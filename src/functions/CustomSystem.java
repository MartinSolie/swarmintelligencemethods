package functions;

/**
 * Class represents system of equations and allows to input each equation as String
 * @author martin
 */
public class CustomSystem implements Computable{
    
    private CustomFunction[] equations;
    private double[] minX;
    private double[] maxX;
    
    public CustomSystem(String[] strEquations, double[] minX, double[] maxX){
        equations = new CustomFunction[strEquations.length];
        
        for (int i = 0; i < strEquations.length; i++){
            equations[i] = new CustomFunction(strEquations[i], minX, maxX);
        }
        
        this.minX = minX;
        this.maxX = maxX;
    }
    
    @Override
    public double getFunctionValue(double[] x) {
        double error = 0;
        
        for (int i = 0; i <  equations.length; i++){
            error += Math.abs(equations[i].getFunctionValue(x));
        }
        
        return error;
    }

    @Override
    public double getMinX(int axis) {
        return minX[axis];
    }

    @Override
    public double getMaxX(int axis) {
        return maxX[axis];
    }

    @Override
    public int getAxesAmount() {
        return minX.length;
    }
    
    public int getEquationsAmount(){
        return equations.length;
    }

    @Override
    public String getName() {
        return "Custom Equation System";
    }
    
    public CustomFunction[] getEquations(){
        return equations;
    }

    @Override
    public void setMinX(double[] newMinX) {
        this.minX = newMinX;        
    }

    @Override
    public void setMaxX(double[] newMaxX) {
        this.maxX = newMaxX;
    }
    
}
