/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

/**
 *
 * @author martin
 */
public class Shekel implements Computable{
    
    private final int m = 10;
    
    private static double[][] c = {
                                    {4, 1, 8, 6, 3, 2, 5, 8, 6, 7},
                                    {4, 1, 8, 6, 7, 9, 3, 1, 2, 3},
                                    {4, 1, 8, 6, 3, 2, 5, 8, 6, 7}                                    
                                  };
    
    private static double[] b = {0.1, 0.2, 0.2, 0.4, 0.4, 0.6, 0.3, 0.7, 0.5, 0.5};
    
    @Override
    public double getFunctionValue(double[] x) {
        
        double result = 0;
        double tmp = 0;
        
        for (int i = 0; i < m; i++){
            tmp = 0;
            for (int j = 0; j < getAxesAmount(); j++){
                tmp += Math.pow((x[j] - c[j][i]), 2.0);
            }
            result += 1.0/(tmp + b[i]);
        }
        
        return -result;
    }

    @Override
    public double getMinX(int axis) {
        return 0;
    }

    @Override
    public double getMaxX(int axis) {
        return 10;
    }

    @Override
    public int getAxesAmount() {
        return 2;
    }

    @Override
    public String getName() {
        return "Shekel";
    }

    @Override
    public void setMinX(double[] newMinX) {
        return;
    }

    @Override
    public void setMaxX(double[] newMaxX) {
        return;
    }
    
    @Override
    public String toString(){
        return " - sum from i = 1 to m (sum from j = 1 to 4 ((xj - Cji)^2 + bi)^-1)";
    }
    
}
