package functions;

/**
 * Class represents task from 
 * Burden R. Numerical Analysis book p 646 task 14
 * @author martin
 */
public class WaterSystem implements Computable {

    @Override
    public double getFunctionValue(double[] x) {
        double error = 0;
        
        double[] y = {2.40, 3.80, 4.75, 21.60};
        double[] z = {31.8, 31.5, 31.2, 30.2};
        double[] sum = new double [6];
        
        for (int i = 0; i < z.length; i++){
            sum[0] += Math.log(y[i]) * y[i] / Math.pow(z[i] - x[1], x[2]);
            sum[1] += 1.0 / Math.pow(z[i] - x[1], 2 * x[2]);
            
            sum[2] += 1.0 / Math.pow (z[i] - x[1], 2 * x[2] + 1);
            sum[3] += Math.log(y[i]) * y[i] / Math.pow(z[i] - x[1], x[2]+1);
            
            sum[4] += Math.log(z[i] - x[1]) / Math.pow (z[i] - x[1], 2 * x[2]);
            sum[5] += Math.log(y[i]) * y[i] * Math.log(z[i] - x[1]) / Math.pow(z[i] - x[1], x[2]);
        }
        
        error += Math.abs(sum[0]/sum[1] - x[0]);
        error += Math.abs(sum[0] * sum[2] - sum[3] * sum[1]);
        error += Math.abs(sum[0] * sum[4] - sum[5] * sum[1]);      
        
        return error;
    }

    @Override
    public double getMinX(int axis) {
        return (axis == 2) ? -9 : -1000;
    }

    @Override
    public double getMaxX(int axis) {
        return (axis == 2) ? 9 : 1000;
    }

    @Override
    public int getAxesAmount() {
        return 3;
    }

    @Override
    public String getName() {
        return "Water System";
    }

    @Override
    public void setMinX(double[] newMinX) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setMaxX(double[] newMaxX) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
}
