/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

/**
 *
 * @author martin
 */
public class Schaffers implements Computable{
    private double[] minX;
    private double[] maxX;
    
    public Schaffers (double[] minX, double[] maxX){
        this.minX = minX;
        this.maxX = maxX;
    }
    
    
    public Schaffers (double minXValue, double maxXValue){
        this.minX = new double[getAxesAmount()];
        this.maxX = new double[getAxesAmount()];
        
        for (int i = 0; i<this.minX.length; i++){
            this.minX[i] = minXValue;
            this.maxX[i] = maxXValue;
        }
    }
    
    @Override
    public double getFunctionValue(double[] x) {
        return 0.5 + (Math.pow(Math.sin(Math.sqrt(x[0]*x[0]+x[1]*x[1])),2)-0.5)/Math.pow((1+0.001*(x[0]*x[0]+x[1]*x[1])),2);        
    }

    @Override
    public double getMinX(int axis) {
        return minX[axis];
    }

    @Override
    public double getMaxX(int axis) {
        return maxX[axis];
    }    

    @Override
    public final int getAxesAmount() {
        return 2;
    }
    
    @Override
    public String getName(){
        return "Schaffers";
    }

    @Override
    public void setMinX(double[] newMinX) {
        this.minX = newMinX;        
    }

    @Override
    public void setMaxX(double[] newMaxX) {
        this.maxX = newMaxX;
    }
    
    @Override
    public String toString(){
        return "0.5 + (sin^2(sqrt(_x0_ ^ 2 + _x1_ ^ 2)) - 0.5) / (1 + 0.001 * (_x0_ ^ 2 + _x1_ ^ 2)) ^ 2";
    }
}
