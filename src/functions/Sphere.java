/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package functions;

/**
 *
 * @author martin
 */
public class Sphere implements Computable {
    private double[] minX;
    private double[] maxX;
    
    public Sphere(double[] minX, double[] maxX){
        this.minX = minX;
        this.maxX = maxX;
    }
    
    public Sphere(double minXValue, double maxXValue){
        this.minX = new double[getAxesAmount()];
        this.maxX = new double[getAxesAmount()];
        
        for (int i = 0; i<this.minX.length; i++){
            this.minX[i] = minXValue;
            this.maxX[i] = maxXValue;
        }
    }
    
    @Override
    public double getFunctionValue(double[] x) {
        return x[0]*x[0]+x[1]*x[1];
    }

    @Override
    public double getMinX(int axis) {
        return minX[axis];
    }

    @Override
    public double getMaxX(int axis) {
        return maxX[axis];
    }   
    
    @Override
    public final int getAxesAmount(){
        return 2;
    }
    
    @Override
    public String getName(){
        return "Sphere";
    }
    
    @Override
    public String toString(){
        return "_x0_ ^ 2 + _x1_ ^ 2";
    }

    @Override
    public void setMinX(double[] newMinX) {
        this.minX = newMinX;        
    }

    @Override
    public void setMaxX(double[] newMaxX) {
        this.maxX = newMaxX;
    }
    
}
