package functions;

/**
 * Class represents task from 
 * Burden R. Numerical Analysis book p 646 task 12
 * @author martin
 */
public class EquationSystem implements Computable{

    @Override
    public double getFunctionValue(double[] x) {
        double error = 0;
        error = Math.abs( x[0] * Math.exp(x[1] * 1) + x[2] * 1 - 10 );
        error += Math.abs( x[0] * Math.exp(x[1] * 2) + x[2] * 2 - 12);
        error += Math.abs( x[0] * Math.exp(x[1] * 3) + x[2] * 3 - 15);
        return error;
    }

    @Override
    public double getMinX(int axis) {
        return (axis == 1) ? 0 : -100;
    }

    @Override
    public double getMaxX(int axis) {
        return 100;
    }

    @Override
    public int getAxesAmount() {
        return 3;
    }

    @Override
    public String getName() {
        return "System solving";
    }

    @Override
    public void setMinX(double[] newMinX) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setMaxX(double[] newMaxX) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
