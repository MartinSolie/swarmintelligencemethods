package mas.gsa;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import mas.AbstractAgent;

/**
 *
 * @author martin
 */
public class GSAAgent extends AbstractAgent {
    private static int amount = 0;
    
    public int id;
    
    private GSASwarm swarm;
    
    public double fValue;
    public double M;
    public double m;
    public double[] F;
    public double[] a;
    public double[] v;
    
    public GSAAgent (GSASwarm swarm){
        this.swarm = swarm;
        
        this.x = new double[swarm.computable.getAxesAmount()];        
        for (int axis = 0; axis < x.length; axis++){
            x[axis] = ThreadLocalRandom.current()
                    .nextDouble(swarm.computable.getMinX(axis), swarm.computable.getMaxX(axis));
        }        
        
        this.F = new double[swarm.computable.getAxesAmount()];
        this.a = new double[swarm.computable.getAxesAmount()];
        this.v = new double[swarm.computable.getAxesAmount()];
        
        id = amount++;
    }
    
    private void updateA(){        
        for (int axis = 0; axis<swarm.computable.getAxesAmount(); axis++){
            if (M == 0) return;
            a[axis] = F[axis]/M;
        }        
    }
    
    private void updateV(){
        for (int axis = 0; axis<swarm.computable.getAxesAmount(); axis++){
            v[axis] = rand.nextDouble() * v[axis] + a[axis];
        }        
    }
    
    private void updateX(){        
        for (int axis = 0; axis<swarm.computable.getAxesAmount(); axis++){
            x[axis] = x[axis] + v[axis];
        }        
    }
    
    public void makeMove() {
        updateA();
        updateV();
        updateX();        
    }
    
}
