package mas.gsa;

import functions.Computable;
import java.util.Random;
import mas.AbstractSwarm;
import mas.MASFactory;

/**
 * Gravitational Search Algorithm
 * @author martin
 */ 
public class GSASwarm extends AbstractSwarm{
    
    public double best;
    public double worst;
    public double g;
    public double gdt;   
    
    public GSASwarm(Computable computable, int N){
        this.g = MASFactory.g;
        this.gdt = MASFactory.gdt;
        
        this.computable = computable;
        this.agents = new GSAAgent[N];
        
        for (int i = 0; i < agents.length; i++){
            agents[i] = new GSAAgent(this);            
        }
        ((GSAAgent)agents[0]).fValue = computable.getFunctionValue(agents[0].x);
        best = ((GSAAgent)agents[0]).fValue;
        gBestX = agents[0].x;
    }
    
    private void evaluateFitness(){
        for (GSAAgent agent : (GSAAgent[])agents){
            agent.fValue = computable.getFunctionValue(agent.x);
        }
    }
    
    private void updateG(){
        g*=gdt;
    }
    
    private void updateBest(){
        for (GSAAgent agent : (GSAAgent[])agents){
            if (agent.fValue<best){
                best = agent.fValue;
                gBestX = agent.x;
            }            
        }
    }
    
    private void updateWorst(){
        for (GSAAgent agent : (GSAAgent[])agents){            
            if (agent.fValue > worst){
                worst = agent.fValue;
            }
        }
    }
    
    private void updatem(){        
        for (GSAAgent agent : (GSAAgent[])agents){            
            agent.m = Math.abs((agent.fValue - worst)/(best - worst));                        
        }        
    }
    
    private void updateM(){
        double sum = 0;
        for (GSAAgent agent : (GSAAgent[])agents){
            sum+=agent.m;            
        }
        for (GSAAgent agent : (GSAAgent[])agents){
            agent.M = agent.m/sum;            
        }        
    }
    
    private void updateF(){
        double R;    
        double[][][] tmpF;
        Random rand = new Random();
        
        tmpF = new double[computable.getAxesAmount()][agents.length][agents.length];
        for (int i = 0; i<agents.length; i++){
            for (int j = 0; j<agents.length; j++){
                if (i == j) continue;
                
                R = 0;
                for (int axis = 0; axis<computable.getAxesAmount(); axis++){
                    R += Math.pow(agents[i].x[axis] - agents[j].x[axis], 2.0);                    
                }
                R = Math.sqrt(R);
                
                if(R==0) continue;
                for (int axis = 0; axis<computable.getAxesAmount(); axis++){
                    tmpF[axis][i][j] = g 
                            * ((GSAAgent) agents[i]).M * ((GSAAgent)agents[j]).M
                            * (agents[j].x[axis]-agents[i].x[axis])
                            / R;
                }
            }
            
            for (int axis = 0; axis < computable.getAxesAmount(); axis++){
                ((GSAAgent)agents[i]).F[axis] = 0;
                for (int j = 0; j<agents.length; j++){
                    if (j==i) continue;
                    ((GSAAgent)agents[i]).F[axis] += rand.nextDouble()*tmpF[axis][i][j];                    
                }
            }
        }
    }
    
    private void updateA(){
        for (GSAAgent agent : (GSAAgent[])agents){
            for (int axis = 0; axis<computable.getAxesAmount(); axis++){
                if (agent.M == 0) continue;
                agent.a[axis] = agent.F[axis]/agent.M;
            }
        }
    }
    
    private void updateV(){
        Random rand = new Random();
        for (GSAAgent agent : (GSAAgent[])agents){
            for (int axis = 0; axis<computable.getAxesAmount(); axis++){
                agent.v[axis] = rand.nextDouble() * agent.v[axis] + agent.a[axis];
            }
        }
    }
    
    private void updateX(){
        for (GSAAgent agent : (GSAAgent[])agents){
            for (int axis = 0; axis<computable.getAxesAmount(); axis++){
                agent.x[axis] = agent.x[axis]+agent.v[axis];
            }
        }
    }
    
    @Override
    public void makeIteration() {
        
        evaluateFitness();
        updateG();
        updateBest();
        updateWorst();
        updatem();
        updateM();
        updateF();

        for (GSAAgent agent : (GSAAgent[])agents){
            agent.makeMove();
        }      
        
        gBest = best;
        iterations++;
    }
    
}
