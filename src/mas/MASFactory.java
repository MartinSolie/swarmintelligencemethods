package mas;

import functions.Computable;
import mas.de.DESwarm;
import mas.gsa.GSASwarm;
import mas.gso.GSOSwarm;
import mas.mso.MSOSwarm;
import mas.pso.PSOSwarm;

/**
 * Stores parameters for each swarm
 * @author martin
 */
public class MASFactory {
    
    public static enum SwarmType { PSO, GSO, DE, MSO, GSA};
    
    //---------------Parameters for Glowworm Swarm Optimizaiton-----------------
    public static double luciferinAmount = 2;      //amount of luciferin at start
    public static double ro = 0.4;                 //luciferin decay constant
    public static double gamma = 0.6;              //luciferin enchancement constant    
    
    public static double beta = 0.08;              //adaptive decision range constant
    public static int nt = 5;                      //desirable amount of neighbours
    public static double rMax = 100;               //maximal decision range
    public static double rDecision = rMax/2;       //decision range of each agent at start
    public static boolean dynamicRange = true;     //whether decision range changes or not    
    
    public static double s = 0.03;                 //step size    
    //--------------------------------------------------------------------------
    
    //------------------Parameters for Particle Swarm Optimization--------------
    public static double kv = 0.6;                         //inertia coefficient
    public static double particleIncrementRatio = 0.03;    //simple nostalgia coefficient
    public static double groupIncrementRatio = 0.03;       //social coefficient
    //--------------------------------------------------------------------------
    
    //----------------Parameters for MultiSwarm Optimization--------------------
    public static int nSwarms = 10;                     //amount of generated swarms
    public static double globalIncrementRatio = 0.01;   //coefficient of interaction between swarms
    public static double msoGroupIncrementRatio = 0.03;       //social coefficient
    public static double msoParticleIncrementRatio = 0.03;    //simple nostalgia coefficient
    public static double msoKV = 0.6;                         //inertia coefficient
    //--------------------------------------------------------------------------
    
    //-----------------Parameters for Differential Evolution--------------------
    public static double F = 1;                    //differential amplification constant
    public static double p = 0.5;                  //crossover constant
    //--------------------------------------------------------------------------
    
    //-------------Parameters for Gravitational Search Algorithm----------------
    public static double g = 0.5;                  //gravitational acceleration
    public static double gdt = 0.95;               //speed of change of g
    //--------------------------------------------------------------------------    
    
    public static AbstractSwarm getSwarm(SwarmType type, Computable computable, int N){
        switch(type){
            case PSO    :  return new PSOSwarm(computable, N);
                
            case GSO    :  return new GSOSwarm(computable, N);
                
            case DE     :  return new DESwarm(computable, N);
                
            case MSO    :  return new MSOSwarm(computable, N);
                
            case GSA    :  return new GSASwarm(computable, N);
            
            default     :  return null;                
        }
    }    
}
