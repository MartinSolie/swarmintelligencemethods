package mas;

import functions.Computable;

/**
 * <code>AbstractSwarm</code> class contains information, similar for all swarms
 * 
 * @author martin
 * @see Worker, AbstractAgent, Computable 
 */
public abstract class AbstractSwarm {
    
    /**
     * Best value ever found by the swarm
     */
    public double gBest;
    
    /**
     * Position of the best value
     */
    public double[] gBestX;
    
    /**
     * Amount of performed iterations
     */
    public long iterations = 0;    
    
    /**
     * Array of swarm's agents
     */
    public AbstractAgent[] agents;
    
    /**
     * Object, which represents problem space
     */
    public Computable computable;
    
    /**
     * Actually the action of each method
     */
    public abstract void makeIteration();
}
