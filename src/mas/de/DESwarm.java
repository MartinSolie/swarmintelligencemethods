package mas.de;

import functions.Computable;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import mas.AbstractSwarm;
import mas.MASFactory;

/**
 * Differential evolution swarm
 * @author martin
 */
public class DESwarm extends AbstractSwarm {

    /**
     * Differential amplification constant
     */
    public final double F; 
    
    /**
     * Crossover constant
     */
    public final double p; 
    
    private final Random rand;
    
    public DESwarm(Computable computable, int N){
        this.F = MASFactory.F;
        this.p = MASFactory.p;
                
        this.computable = computable;
        this.agents = new DEAgent[N];
        
        for (int i = 0; i<N; i++){
            agents[i] = new DEAgent(this);
        }
        
        gBest = computable.getFunctionValue(agents[0].x);        
        gBestX = agents[0].x;
        
        rand = new Random();
    }
        
    @Override
    public void makeIteration() {
        int first, second, third;
        double decision;
        
        for (int i = 0; i<agents.length; i++){
            
            //choosing vectors for mutation ====================================
            do{
                first = ThreadLocalRandom.current().nextInt(0, agents.length);
            } while (first == i);
            
            do{
                second = ThreadLocalRandom.current().nextInt(0, agents.length);
            } while (second == i || second == first);
            
            do{
                third = ThreadLocalRandom.current().nextInt(0, agents.length);
            } while (third == second || third == first || third == i);
            //==================================================================
            
            //performing crossover =============================================
            double[] trialX = new double[computable.getAxesAmount()];
            for (int axis = 0; axis<trialX.length; axis++){
                decision = rand.nextDouble();
                if (decision<p){                    
                    trialX[axis] = agents[first].x[axis] 
                            + F * (agents[second].x[axis]-agents[third].x[axis]);
                    
                    if (trialX[axis]<computable.getMinX(axis)) trialX[axis] = computable.getMinX(axis);
                    if (trialX[axis]>computable.getMaxX(axis)) trialX[axis] = computable.getMaxX(axis);
                } else {
                    trialX[axis] = agents[i].x[axis];
                }
            }
            //==================================================================
            
            //performing selection =============================================
            if (computable.getFunctionValue(trialX)<computable.getFunctionValue(agents[i].x)){
                agents[i].x = trialX;
                if (computable.getFunctionValue(trialX)<gBest){
                    gBest = computable.getFunctionValue(trialX);
                    gBestX = trialX;
                }
            }
            //==================================================================
        }
        iterations++;
        
        
    }    
}
