package mas.de;

import java.util.concurrent.ThreadLocalRandom;
import mas.AbstractAgent;

/**
 * @author martin
 */
public class DEAgent extends AbstractAgent {
    
    public DEAgent(DESwarm swarm){        
        x = new double[swarm.computable.getAxesAmount()];
        
        for (int i = 0; i<x.length; i++){
            x[i] = ThreadLocalRandom.current()
                    .nextDouble(swarm.computable.getMinX(i), swarm.computable.getMaxX(i));
        }
        
        
    }
    
}
