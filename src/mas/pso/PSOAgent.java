package mas.pso;

import functions.Computable;
import java.util.concurrent.ThreadLocalRandom;
import mas.AbstractAgent;

/**
 * 
 * @author martin
 */
public class PSOAgent extends AbstractAgent {    
    
    /**
     * Velocity vector
     */
    public double[] vx;    
    
    /**
     * Coordinates of best position ever found by particle
     */
    public double[] bestX;    
    
    /**
     * Best value ever found by particle
     */
    public double bestValue;
    
    public PSOAgent(Computable computable){       
        x = new double[computable.getAxesAmount()];
        vx = new double[computable.getAxesAmount()];
        bestX = new double[computable.getAxesAmount()];
        
        for (int axis = 0; axis < x.length; axis++){
            x[axis] = ThreadLocalRandom.current().nextDouble(computable.getMinX(axis), computable.getMaxX(axis));
            bestX[axis] = x[axis];
        }
                
        bestValue = computable.getFunctionValue(x);
    }   
        
}
