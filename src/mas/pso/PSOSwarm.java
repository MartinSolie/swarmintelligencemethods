package mas.pso;

import functions.Computable;
import java.util.Random;
import mas.AbstractSwarm;
import mas.MASFactory;

/**
 * Particle Swarm Optimization
 * @author martin
 */
public class PSOSwarm extends AbstractSwarm {
    public static final double V_START = 0.6;
    public static final double V_END = 0.1;    
    
    /**
     * inertia coefficient
     */
    public double kv;                       
    
    /**
     * simple nostalgia coefficient
     */
    public double particleIncrementRatio;   
    
    /**
     * social coefficient
     */
    public double groupIncrementRatio;      
    
    private final Random rand;
    
    public PSOSwarm(Computable computable, int N){
        this.kv = MASFactory.kv;
        this.particleIncrementRatio = MASFactory.particleIncrementRatio;
        this.groupIncrementRatio = MASFactory.groupIncrementRatio;
        
        rand = new Random();
        
        agents = new PSOAgent[N];        
        this.computable = computable;
        gBestX = new double[computable.getAxesAmount()];        
        
        for (int i = 0; i<N; i++){
            agents[i] = new PSOAgent(computable);
            PSOAgent agent = (PSOAgent)agents[i];            
        }       
        
        gBest = ((PSOAgent)agents[0]).bestValue;
        gBestX = agents[0].x;        
    }    
    
    @Override
    public void makeIteration() {
        double currentValue;
        
        for (PSOAgent agent : (PSOAgent[])agents){            
            
            for (int axis = 0; axis < agent.x.length; axis++) {
                agent.vx[axis] = kv * agent.vx[axis]
                        + particleIncrementRatio * rand.nextDouble() * (agent.bestX[axis] - agent.x[axis])
                        + groupIncrementRatio * rand.nextDouble() * (gBestX[axis] - agent.x[axis]);
                agent.x[axis] += agent.vx[axis];
            }
                
            currentValue = computable.getFunctionValue(agent.x);
            
            if (currentValue < agent.bestValue) {
                agent.bestValue = currentValue;
                agent.bestX = agent.x;
            }
            
            if (gBest > agent.bestValue){
                gBest = agent.bestValue;
                gBestX = agent.bestX;
            }
        }     
        iterations++;             
    }    
}
