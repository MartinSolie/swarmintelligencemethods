/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.mso;

import functions.Computable;

/**
 *
 * @author martin
 */
public class MSOSubSwarm {
    
    public double[] groupBestX;
    public double bestValue;
    
    public MSOSubSwarm(MSOSwarm swarm, int nAgents, int index){
        for (int i = index; i < index+nAgents; i++){
            swarm.agents[i] = new MSOAgent(swarm, this);
        }
        bestValue = ((MSOAgent)swarm.agents[index]).bestValue;
        groupBestX = ((MSOAgent)swarm.agents[index]).bestX;
    }    
}
