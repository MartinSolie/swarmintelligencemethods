/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.mso;

import functions.Computable;
import mas.AbstractSwarm;
import mas.MASFactory;

/**
 * Multi Swarm Optimization
 * @author martin
 */
public class MSOSwarm extends AbstractSwarm{
    
    public static final double V_START = 0.5;
    public static final double V_END = 0.1;    
    
    public double kv;    
    public double particleIncrementRatio = 0.3;
    public double groupIncrementRatio = 0.3;
    public double globalIncrementRatio = 0.03;
            
    private MSOSubSwarm[] swarms;
    
    public MSOSwarm(Computable computable, int nAgents){
        this.kv = MASFactory.kv;
        this.particleIncrementRatio = MASFactory.msoParticleIncrementRatio;
        this.groupIncrementRatio = MASFactory.msoGroupIncrementRatio;
        this.globalIncrementRatio = MASFactory.globalIncrementRatio;       
        
        this.computable = computable;
        swarms = new MSOSubSwarm[MASFactory.nSwarms];
        agents = new MSOAgent[nAgents*MASFactory.nSwarms];
        iterations = 0;
        
        for (int i = 0; i < MASFactory.nSwarms; i++){
            swarms[i] = new MSOSubSwarm(this, nAgents, i*nAgents);
        }
        
        gBest = ((MSOAgent)agents[0]).bestValue;
        gBestX = ((MSOAgent)agents[0]).bestX;
    }
    
    
    @Override
    public void makeIteration() {
        for (MSOAgent agent : (MSOAgent[])agents){
            agent.makeMove();
        }
        iterations++;
    }
}
