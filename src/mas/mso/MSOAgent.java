/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.mso;

import java.util.concurrent.ThreadLocalRandom;
import mas.AbstractAgent;
import mas.pso.PSOSwarm;

/**
 *
 * @author martin
 */
public class MSOAgent extends AbstractAgent{
    
    private MSOSwarm swarm;
    private MSOSubSwarm subSwarm;
    
    public double[] vx;
    
    public double[] bestX;    
    public double bestValue;
    
    public MSOAgent(MSOSwarm swarm, MSOSubSwarm subSwarm){        
        this.swarm = swarm;
        this.subSwarm = subSwarm;        
        
        x = new double[swarm.computable.getAxesAmount()];
        vx = new double[swarm.computable.getAxesAmount()];
        bestX = new double[swarm.computable.getAxesAmount()];
        
        for (int axis = 0; axis < x.length; axis++){
            x[axis] = ThreadLocalRandom.current().nextDouble(swarm.computable.getMinX(axis), swarm.computable.getMaxX(axis));
            bestX[axis] = x[axis];
        }
                
        bestValue = swarm.computable.getFunctionValue(x);       
    }   
        
    public void makeMove() {
        
        for (int axis = 0; axis < x.length; axis++){
            vx[axis]=swarm.kv * vx[axis]
                + swarm.particleIncrementRatio * rand.nextDouble() * (bestX[axis]-x[axis])
                + swarm.groupIncrementRatio * rand.nextDouble() * (subSwarm.groupBestX[axis]-x[axis]);
                //+ swarm.globalIncrementRatio * rand.nextDouble() * (swarm.gBestX[axis]-x[axis]); //comment this line to make multi object optimization
            x[axis] += vx[axis];
        }       
                
        double evaluate = swarm.computable.getFunctionValue(x);
        
        if (evaluate < bestValue){
            bestValue = evaluate;
            bestX = x;
        } 
        
        if (evaluate < subSwarm.bestValue){
            subSwarm.bestValue = evaluate;
            subSwarm.groupBestX = x;
        }
        
        if (evaluate < swarm.gBest){
            swarm.gBest = evaluate;
            swarm.gBestX = x;
        }
    }    
}
