package mas.gso;

import functions.Computable;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import mas.AbstractAgent;
import mas.MASFactory;

/**
 *
 * @author martin
 */
public class GSOAgent extends AbstractAgent {
    /**
     * Amount of luciferin
     */
    private double luciferin;
    
    /**
     * Decision range of glow worm
     */
    private double rDecision;
    
    /**
     * Link to glow worm's swarm
     */
    private GSOSwarm swarm;
    
    /**
     * List of agents, who are in decision range of glow worm 
     */
    private ArrayList<GSOAgent> neighbours;
    
    /**
     * Probability to move toward each neighbor
     */
    private double[] probabilities;    
    
    public GSOAgent(GSOSwarm swarm, double luciferin, double rDecision){        
        this.luciferin = luciferin;
        this.rDecision = rDecision;
        this.swarm = swarm;
        
        x = new double[this.swarm.computable.getAxesAmount()];
        
        for (int i = 0; i< this.swarm.computable.getAxesAmount(); i++){
            x[i] = ThreadLocalRandom.current().nextDouble(this.swarm.computable.getMinX(i), this.swarm.computable.getMaxX(i));
        }
    }
    
    public double getLuciferin(){
        return luciferin;
    }
    
    public double getR(){
        return rDecision;
    }
    
    /**
     * Changes the position of glow worm
     * The rule of movement is:
     * x(t+1)=x(t) + (xj(t)-xi(t)/sqrt(xj(t)^2-xi(t)^2))
     */
    public void makeMove() {        
        calculateProbabilities();
        
        double decision = rand.nextDouble();
        double probability = 0.0;
        int i;
        for (i = 0; probability<decision && i<probabilities.length; i++){
            probability+=probabilities[i];
        }
        
        GSOAgent neighbour = neighbours.get(i-1);
        for (int axis = 0; axis < x.length; axis++){
            if (Math.abs(neighbour.x[axis] - x[axis]) > 1e-9){
                x[axis] = x[axis] + swarm.s * ((neighbour.x[axis] - x[axis]) / Math.sqrt(Math.pow(neighbour.x[axis] - x[axis],2.0)));
                if (x[axis] > swarm.computable.getMaxX(axis))
                    x[axis] = swarm.computable.getMaxX(axis);
                else if (x[axis] < swarm.computable.getMinX(axis))
                    x[axis] = swarm.computable.getMinX(axis);
            } else {
                //x[axis] = x[axis]-0.1+0.2*rand.nextDouble();
            }
        }
        
        updateLuciferin();        
    }
    
    /**
     * Computes probability to move to each neighbor
     * The rule is:
     * lj(t) - li(t) / sum(lj(t)-li(t));
     */
    public void calculateProbabilities(){       
        probabilities = new double [neighbours.size()];
        double sum = 0.0;
        
        for (GSOAgent neighbour : neighbours){
            sum += neighbour.getLuciferin() - this.luciferin;
        }
        
        for (int i = 0; i < neighbours.size(); i++){
            probabilities[i] = neighbours.get(i).getLuciferin() / sum;
        }
    }
    
    /**
     * Changes the value of luciferin 
     * The rule is:
     * lj(t+ 1) = (1 − ρ) lj (t) + γJj (t + 1)
     * where
     * l(t) - amount of luciferin at time t
     * ro - luciferin decay constant
     * gamma - luciferin enchancement constant
     * J(t) - objective function value at time t
     */
    public void updateLuciferin(){               
        luciferin = (1 - swarm.ro)*luciferin + swarm.gamma * swarm.computable.getFunctionValue(x);
    }
    
    public void updateNeighbours(ArrayList<GSOAgent> neighbours){
        this.neighbours = neighbours;
    }
    
    /**
     * Changes decision range
     * The rule is:
     * r(t+1) = min(rMax, max(0, r(t) + beta(nt - |N(t)|)))
     * where
     * r(t+1) - decision range at time moment t+1
     * r(t) - decision range at time moment t
     * rMax - maximum decision range
     * beta - constant
     * nt - constant, defining desirable amount of neighbours in decision range
     * N(t)| - amount of neighbours with current decision range
     */
    public void updateDecisionRange(){
       rDecision = Math.min(swarm.rMax, Math.max(0, rDecision + swarm.beta * (swarm.nt - neighbours.size())));
    }
}
