package mas.gso;

import functions.Computable;
import java.util.ArrayList;
import mas.AbstractAgent;
import mas.AbstractSwarm;
import mas.MASFactory;

/**
 * GlowWorm Swarm Optimization
 * @author martin
 */
public class GSOSwarm extends AbstractSwarm{
    
    /**
     * luciferin decay constant 0<ro<1
     */
    public double ro;
    
    /**
     * luciferin enchancement constant
     */
    public double gamma;
    
    /**
     * step size
     */
    public double s;
    
    public double minS = 1e-4;
    public double ds = 0.9999;
    
    /**
     * constant for adaptive decision
     */
    public double beta;
    
    /**
     * desirable amount of neighbors in decision range
     */
    public int nt;
    
    /**
     * maximal decision range
     */
    public double rMax; 
    
    /**
     * Sets whether decision range will be updated after each iteration
     */
    public boolean dynamicRange;
                
    private int N;   
    
    public GSOSwarm (Computable computable, int N){        
        this.ro = MASFactory.ro;
        this.gamma = MASFactory.gamma;
        this.s = MASFactory.s;
        this.beta = MASFactory.beta;
        this.nt = MASFactory.nt;
        this.rMax = MASFactory.rMax;
        this.dynamicRange = MASFactory.dynamicRange;
        
        this.computable = computable;       
        
        this.N = N;
        this.agents = new GSOAgent[N];
        
        for (int i = 0; i<N; i++){
            agents[i] = new GSOAgent(this, MASFactory.luciferinAmount, MASFactory.rDecision);            
        } 
        
        gBest = computable.getFunctionValue(agents[0].x);
        gBestX = agents[0].x;
    } 
    
    /**
     * For each agent the swarm creates list of agents who are in it's decision range
     */
    public final void updateEverybodiesNeighbours(){
        GSOAgent[] gsoagents = (GSOAgent[])agents;
        int axesAmount = computable.getAxesAmount();
        
        for (int i = 0; i<N; i++){            
            
            ArrayList<GSOAgent> neighbours = new ArrayList<>();
            for (int j = 0; j<N; j++){
                if (i == j)
                    continue;
                
                double distance = 0;
                for (int axis = 0; axis<axesAmount; axis++){
                    distance += Math.pow(gsoagents[j].getX(axis)-gsoagents[i].getX(axis),2.0);
                }
                distance = Math.sqrt(distance);
                
                if (distance < gsoagents[i].getR()
                    && (gsoagents[j].getLuciferin() < gsoagents[i].getLuciferin())){
                    neighbours.add(gsoagents[j]);
                }
            }
            
            if (neighbours.isEmpty()){
                neighbours.add(gsoagents[i]);
            } 
            
            gsoagents[i].updateNeighbours(neighbours);             
        }
        
        if (dynamicRange){
            for (GSOAgent agent : gsoagents){
                agent.updateDecisionRange();
            }
        }
    }
    
    @Override
    public void makeIteration() {
        updateEverybodiesNeighbours();
        
        for (AbstractAgent agent : agents){
            ((GSOAgent)agent).makeMove();
            if (gBest>computable.getFunctionValue(agent.x)){
                gBest = computable.getFunctionValue(agent.x);
                gBestX = agent.x;
            }            
        }
        
        iterations++;
        
        //s = (ds * s <= minS) ? minS : ds * s; //for dynamic speed - uncomment this line
    }
}
