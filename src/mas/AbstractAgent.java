package mas;

import java.util.Random;

/**
 * @author martin
 */
public abstract class AbstractAgent {    
   
    /**
     * Position of agent in problem space
     */
    public double[] x;    
    
    protected static final Random rand = new Random();    
    
    public double getX(int axis){
        return x[axis];
    }
    
    public int getAxesAmount(){
        return x.length;
    }      
}
